package edu.pucmm;

import edu.pucmm.rutas.Inicio;
import edu.pucmm.model.RouteBase;
import edu.pucmm.rutas.Ajax;
import spark.Spark;


public class Main {

    public static void main(String[] args) {
        Spark.port(8060);
        addRoute(new Inicio());
        addRoute(new Ajax());
        Spark.init();
    }

    private static void addRoute(RouteBase routeBase) {
        Spark.get(routeBase.path, routeBase);
    }
}
