package edu.pucmm.rutas;

import edu.pucmm.model.RouteBase;
import spark.Request;
import spark.Response;



public class Inicio extends RouteBase {

    public Inicio() {
        super("/");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        return " Hello world ";
    }
}
